import ko from 'knockout';
import template from './app.template.html';
import viewModel from './app.viewmodel.js';
import './app.styles.scss'; //Inkludere style hvis der er noget til det pågælende component.

import '../increment'//Load increment component
import '../dummy'; //Load Dummy component

ko.components.register('app', { template, viewModel });
