
import 'bootstrap/dist/css/bootstrap.css';

import ko from 'knockout';

//Import knockout componenets
import './components/app';
//Start Knockout
ko.deferUpdates = true;
ko.applyBindings();
