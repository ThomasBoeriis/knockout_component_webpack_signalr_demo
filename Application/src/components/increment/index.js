import ko from 'knockout';
import template from './increment.template.html';
import viewModel from './increment.viewmodel.js';

//Register component med tilhørende template og viewmodel
ko.components.register('increment', { template, viewModel });
