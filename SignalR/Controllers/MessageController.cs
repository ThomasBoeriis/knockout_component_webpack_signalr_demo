﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using SignalRChat.Hubs;

namespace SignalRChat.Controllers
{
    public class MessageController : Controller
    {

        private readonly IHubContext<MoveShapeHub> _hubContext;

        public MessageController(IHubContext<MoveShapeHub> hubContext)
        {
            _hubContext = hubContext;
        }

        [Route("home/index")]
        public async Task<IActionResult> Index(string message)
        {
            await _hubContext.Clients.All.SendAsync("reciveMessage", message);
            return Content("OK");
        }
    }
}