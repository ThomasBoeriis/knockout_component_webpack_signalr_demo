import ko from 'knockout';
//Import jQuery Plugins
import $ from 'jquery';
import Packery from 'packery';
import jQBridget from 'jquery-bridget';
import Draggabilly from 'draggabilly';
function appViewModel() {
    const vm = {};

    
    vm.draggable = ko.observable(false); //Kan vi drag vores elementer?
    var draggies = []; //Alle Draggies elementer

    //Click funktion  til toggle om vi kan drag eller ej.
    vm.toggleDrag = function () {
        // switch flag
        vm.draggable(!vm.draggable());
        var method = vm.draggable() ? 'enable' : 'true';
        draggies.forEach( function( draggie ) {
          draggie[method]();
        });
    }

    vm.init = function () {
        /*
            Implementering af Packery && dragabilly
            Packery er et sorterings plugin som sortere elementer og pakker dem så de passer bedst muligt i et grid.
            Dragabilly er et plugin der gør det nemt og lave drag & drop funktionalitet.
        */
        //når vi bruger webpack bruger vi jQBridget til at smide vores plugins ind i jquery
        jQBridget('packery', Packery, $);
        jQBridget('draggabilly', Draggabilly, $);

        //Init Packery
        var $grid = $('#dashboard-elements').packery({
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            //rowHeight: 190,
            percentPosition: true,
            isInitLayout: false
        });
        //Variable til at finde data options på packery
        var pckry = $grid.data('packery');

        // ----- initial sort ----- //
        var sortOrder = []; // global variable for saving order, used later
        var storedSortOrder = localStorage.getItem('sortOrder'); //vi bruger local storage til at gemme hvad for en sortering elementerne kommer i.
        if (storedSortOrder) {
            storedSortOrder = JSON.parse(storedSortOrder);
            // create a hash of items by their tabindex
            var itemsByTabIndex = {};
            var tabIndex;
            for (var i = 0, len = pckry.items.length; i < len; i++) {
                var item = pckry.items[i];
                tabIndex = $(item.element).attr('tabindex');
                itemsByTabIndex[tabIndex] = item;
            }
            // overwrite packery item order
            i = 0; len = storedSortOrder.length;
            for (; i < len; i++) {
                tabIndex = storedSortOrder[i];
                pckry.items[i] = itemsByTabIndex[tabIndex];
            }
        }

        // ----- packery setup ----- //

        // Trigger packery til at pak elementerne sammen
        $grid.packery();

        var itemElems = $grid.packery('getItemElements');
        // for each item element
        $(itemElems).each(function (i, itemElem) {
            // make element draggable with Draggabilly
            var draggie = new Draggabilly(itemElem, {
                handle: '.handle'
            });
            //Skal elementerne være draggable til at starte med.?
            if (vm.draggable()) {
                draggie.enable();
            }
            else {
                draggie.disable();
            }
            draggies.push(draggie);
            // bind Draggabilly events to Packery
            $grid.packery('bindDraggabillyEvents', draggie);
        });

        //Funktion til at gemme sotreringen i localstorage
        function orderItems() {
            var itemElems = pckry.getItemElements();
            // reset / empty oder array
            sortOrder.length = 0;
            for (var i = 0; i < itemElems.length; i++) {
                sortOrder[i] = itemElems[i].getAttribute("tabindex");
            }
            // save tabindex ordering
            localStorage.setItem('sortOrder', JSON.stringify(sortOrder)); //Gem i localstorage
        }

        //Ved layout complete og et element er blevet dragged og dropped kør OrderItems
        $grid.packery('on', 'layoutComplete', orderItems);
        $grid.packery('on', 'dragItemPositioned', function () {
            orderItems;
            // $grid.packery();
        });
    }

    //Kør init funktion.
    vm.init();

    return vm;
}
//Husk og eksportere viewmodelen så vi kan import den bagefter.
export default appViewModel;
