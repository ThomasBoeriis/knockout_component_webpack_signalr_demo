﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace SignalRChat.Hubs
{
    public static class UserHandler //this static class is to store the number of users conected at the same time
    {
        public static HashSet<string> ConnectedIds = new HashSet<string>();
    }

    public class MoveShapeHub : Hub
    {

        public async Task MoveShape(int x, int y) // this method will be called from the client, when the user drag/move the shape
        {
            var caller = Clients.Caller;

            await Clients.Others.SendAsync("MoveShape", x, y);// this method will send the coord x, y to the other users but the user draging the shape
        }

        public async Task SendMessage(string  message) // this method will be called from the client, when the user drag/move the shape
        {
            
            var caller = Context.ConnectionId;
            await Clients.Client(Context.ConnectionId).SendAsync("reciveMessage", $"Du skrev: {message}");// this method will send the coord x, y to the other users but the user draging the shape

            await Clients.Others.SendAsync("reciveMessage",$"{caller} skrev: {message}");// this method will send the coord x, y to the other users but the user draging the shape
        }
        public override Task OnConnectedAsync()
        {
            UserHandler.ConnectedIds.Add(Context.ConnectionId); //add a connection id to the list
            Clients.All.SendAsync("usersConnected",UserHandler.ConnectedIds.Count()); //this will send to ALL the clients the number of users connected

            return base.OnConnectedAsync();
        }
        public override Task OnDisconnectedAsync(Exception exception)
        {
            UserHandler.ConnectedIds.Remove(Context.ConnectionId);
            Clients.All.SendAsync("usersConnected", UserHandler.ConnectedIds.Count()); //this will send to ALL the clients the number of users connected
            return base.OnDisconnectedAsync(exception);
        }
    }
}
