
# Knockout Webpack med SignalR demo

**Dette er et proof of concept til en projekt**

## Application
`npm install` i application dir for at installere nødvendige moduler i node
`npm run build` til at build webpack

Projektet består af følgende dependencies
- bootstrap
- knockout
- draggabilly
- jquery
- jquery-bridget
- jquery-ui
- packery
- @aspnet/signalr

Dev dependencies

- autoprefixer
- babel-core
- babel-loader
- babel-preset-env
- core-js
- css-loader
- eslint
- eslint-config-spreetail
- extract-text-webpack-plugin
- file-loader
- html-loader
- node-sass
- postcss-loader
- sass-loader
- style-loader
- url-loader
- webpack

## SignalR Server
Åben projektet og kør det så burde applikationen have adgang til at tilgå projektet.