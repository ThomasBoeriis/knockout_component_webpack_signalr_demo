import $ from 'jquery';
import ko from 'knockout';
import 'jquery-ui/ui/widgets/draggable';
import * as signalR from '@aspnet/signalr';

function dummyWidgetViewModel() {
    var self = this;
    self.connectedUsers = ko.observable(); //Observable til at store alle connected users

    self.message = ko.observable();
    self.output = ko.observable();

    var $shape = $('#shape'); //Boksen vi kan flytte rundt på.

    //Build en connetion til vores signalR Hub
    var connection = new signalR.HubConnectionBuilder()
        .withUrl("http://localhost:50082/moveshapehub")
        .build();
    
    //Subscriptions til vores hub der kan sende ud til bestemte endpoints
    connection.on("usersConnected", (userCount) => {
        self.connectedUsers(userCount);
    });

    connection.on("MoveShape", function (x, y) {
        $shape.css({ left: x, top: y }); //this moves the shape in the clients to the coords x, y 
    });

    connection.on("reciveMessage", function (message) {
        self.output(message);
    });


    //Start en connection til vores Hub

    connection.start();
    
    //Gør det muligt og drag vores shape
    $shape.draggable({
        containment: "parent",
        drag: function () { 
            //når en brugere dragger shapen, sender vi vores position til Hubben med x og y values.
            var pos = $(this).position();
            connection.invoke('MoveShape', pos.left, pos.top).catch(function (err) {
                return console.error(err.toString());
            });
        }
    });

    //Send besked funktion, send beskeden til hubben så hubben kan manipulere med beskeden.
    self.sendMessage = function () {
        console.log("Sender Besked");
        connection.invoke('SendMessage', self.message()).catch(function (err) {
            return console.error(err.toString());

        });
    }
}

export default dummyWidgetViewModel;