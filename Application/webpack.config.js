const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
    devtool: 'source-map',
    entry: [
        './src/ko-todo.module.js',
        'core-js/fn/promise' //Er nødvendig til pollyfill for IE
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            //Babel-loader nødvendig for es6 compilering
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            //Optional: Hvis vi vil køre med scss filer
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader', 'sass-loader']
                })
            },
            //Loader css filer
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader']
                })
            },
            //Loader url loader fonts og svg
            {
                test: /\.(eot|svg|ttf|woff(2)?)(\?v=\d+\.\d+\.\d+)?/,
                use: ['url-loader']
            },
            //Html loader nødvendig for at strukturere components i knockout
            {
                test: /\.html/,
                use: ['html-loader']
            }
        ]
    },
    //Extract plugin til css
    plugins: [
        new ExtractTextPlugin('site.css')
    ]
};

module.exports = config;
