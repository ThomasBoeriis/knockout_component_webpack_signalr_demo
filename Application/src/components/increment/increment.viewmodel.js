import ko from 'knockout';

//Params kommer fra params på DOM elementet
function ViewModel(params) {
    this.value = ko.observable(parseInt(params.value));

    //Kør funktion med interval
    //Denne lægger et tal til den nuværende værdi
    setInterval(() => {
        var currentVal = this.value();
        currentVal++;
        this.value(currentVal);
    }, params.interval);
}

export default ViewModel;