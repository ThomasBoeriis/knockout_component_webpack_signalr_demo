import ko from 'knockout';
import template from './dummy-widget.template.html';
import viewModel from './dummy-widget.viewmodel.js';

import './dummy-widget.style.scss';

ko.components.register('dummywidget', { template, viewModel });
